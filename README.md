# README #

### What is this repository for? ###

* This is the Ansible script for deploying Nodejs servers.
* 0.1

### How do I get set up? ###

The application contains a variable file inder group_vars called all.yml, this file contains the variables
that need to be filled out in order for the application to be setup and installed on your ubuntu server.

Please note that this playbook is designed to be used with a Jenkins or Ansible control node, this will not 
execute correctly if run on the instance you are trying to configure.

### Who do I talk to? ###

* Beau Johnson or Prathyusha Sama
* Australian Medical Council DevOps Team