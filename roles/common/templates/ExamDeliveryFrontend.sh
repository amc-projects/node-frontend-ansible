#!/bin/bash

process=$1
PID_FILE="/home/ubuntu/tmp/ExamDeliveryFrontend.pid"
case $process in
    start)
        echo "Starting ExamDelivery Frontend"
        #sudo su - ubuntu
        cd /home/ubuntu/ExamDeliveryFrontend/
        #source venv/bin/activate

        npm start 2>/dev/null &
        echo $! > $PID_FILE
        ;;
    stop)
        #kill -9 $(cat $PID_FILE)
        sudo fuser -ku 8000/tcp
        rm $PID_FILE
        ;;

    *)
        echo "INVALID OPTION"
        ;;
esac


