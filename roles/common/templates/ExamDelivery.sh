#!/bin/bash

process=$1
PID_FILE="/home/amcadmin/tmp/ExamDelivery.pid"
case $process in
    start)
        echo "Starting ExamDelivery"
        #sudo su - ubuntu
        cd /home/amcadmin/ExamDelivery/
        #source venv/bin/activate
       
        npm start 2>/dev/null &
        echo $! > $PID_FILE
        ;;
    stop)
        #kill -9 $(cat $PID_FILE)
        sudo fuser -ku 8000/tcp
        rm $PID_FILE
        ;;

    *)
        echo "INVALID OPTION"
        ;;
esac



